<section class="banner-container">
    <div style="background-image: url('<?php echo INCLUDE_PATH; ?>images/bg.jpeg');" class="banner-single"></div><!--banner-single-->
    <div style="background-image: url('<?php echo INCLUDE_PATH; ?>images/bg2.jpg');" class="banner-single"></div><!--banner-single-->
    <div style="background-image: url('<?php echo INCLUDE_PATH; ?>images/bg3.jpg');" class="banner-single"></div><!--banner-single-->
        <div class="overlay"></div><!--overlay-->
            <div class="center">
                <form>
                    <h2>Qual o seu melhor e-mail?</h2>
                    <input type="email"name="email" required/>
                    <input type="submit"name="acao" value="Cadastrar"/>
                </form>
            </div><!--center-->
    </section><!--banner-principal-->
    
    <section class="descricao-autor">
        <div class="center">
            <div class="w50 left">
                <h2>Vinicius dos Santos</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros.</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros.</p>
            </div><!--w50-->

            <div class="w50 left">
                <img class="right" src="<?php echo INCLUDE_PATH;?>images/foto.png"/>
            </div><!--w50-->
            <div class="clear"></div>
        </div><!--center-->
    </section><!--descricao-autor-->

    <section class="especialidades">
        <div class="center">
            <h2 class="title">Especialidades</h2>
            <div class="w33 left box-especialidade">
                <h3><i class="fab fa-css3-alt"></i></h3>
                <h4>CSS3</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros.</p>
            </div><!--box-especialidade-->

            <div class="w33 left box-especialidade">
                <h3><i class="fab fa-html5"></i></h3>
                <h4>HTML5</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros.</p>
            </div><!--box-especialidade-->

            <div class="w33 left box-especialidade">
                <h3><i class="fab fa-js-square"></i></h3>
                <h4>JavaScript</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros.</p>
            </div><!--box-especialidade-->
        </div><!--center-->
        <div class="clear"></div><!--clear-->
    </section><!--especialidades-->

    <section class="extras">
        <div class="center">
            <div id ="depoimentos" class="w50 left depoimentos-container">
                <h2 class="title">Depoimentos dos nossos clientes</h2>
                <div class="depoimento-single">
                    <p class="depoimento-descricao">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros."</p>
                    <p class="nome-autor">Lorem Ipsum</p>
                </div><!--depoimento-single-->

                <div class="depoimento-single">
                    <p class="depoimento-descricao">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros."</p>
                    <p class="nome-autor">Lorem Ipsum</p>
                </div><!--depoimento-single-->

                <div class="depoimento-single">
                    <p class="depoimento-descricao">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros."</p>
                    <p class="nome-autor">Lorem Ipsum</p>
                </div><!--depoimento-single-->
            </div><!--w50-->

            <div id="servicos" class="w50 left servicos-container">
                <h2 class="title">Serviços</h2>
                <div class="servicos">
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros.</li>

                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros.</li>

                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique leo ut tortor imperdiet tristique. Nam non mi commodo, euismod velit sed, aliquet diam. Nam placerat lorem ac leo interdum, vel dapibus ligula sollicitudin. Mauris dictum odio sit amet libero tristique imperdiet. Aenean et lectus arcu. Phasellus maximus ac ligula vitae lobortis. Curabitur eu elit eros.</li>
                    </ul>
                </div><!--servicos-->
            </div><!--w50-->
            <div class="clear"></div>
        </div><!--center-->
    </section><!--extras-->