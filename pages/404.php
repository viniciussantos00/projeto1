<section class="erro-404">
    <div class="center">
        <div class="wrapper-404">
            <img src="<?php echo INCLUDE_PATH?>images/erro404.gif"alt="Erro 404"></br></br>
            <h2><i style="padding:0 10px;" class="fa fa-times"></i>A página não existe!</h2>
            <p>Deseja voltar para a <a href="<?php echo INCLUDE_PATH?>">página inicial</a>?</p>
        </div><!--wrapper-404-->
    </div><!--center-->
</section>