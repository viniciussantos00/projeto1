<?php include('config.php');?>
<!DOCTYPE html>
<html>
    <head>
        <title>Projeto 1</title>
        <link href="<?php echo INCLUDE_PATH;?>estilo/css/all.css" rel="stylesheet"> <!--load all styles -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap" rel="stylesheet">
        <link href="<?php echo INCLUDE_PATH;?>estilo/style.css" rel="stylesheet"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Descrição do meu site">
        <meta name="keywords" content="palavras,chave,do,site">
        <meta charset="UTF-8"/>
    </head>
    <body>
        <?php
            $url = isset($_GET['url']) ? $_GET['url'] : 'home';
            switch($url){
                case 'depoimentos':
                    echo '<target target="depoimentos"/>';
                break;
                case 'servicos':
                    echo '<target target="servicos"/>';
                break;
                
            }
        
        ?>
        <header>
            <div class="center">
                <div class="logo left"><a href="<?php echo INCLUDE_PATH;?>">Inotech Soluções</a></div><!--logo-->
                <nav class="desktop right">
                    <ul>
                        <li><a href="<?php echo INCLUDE_PATH;?>">Home</a></li>
                        <li><a href="<?php echo INCLUDE_PATH;?>depoimentos">Depoimentos</a></li>
                        <li><a href="<?php echo INCLUDE_PATH;?>servicos">Serviços</a></li>
                        <li><a href="<?php echo INCLUDE_PATH;?>contato">Contato</a></li>
                    </ul>
                </nav><!--desktop-->

                <nav class="mobile right">
                    <div class="botao-menu-mobile">
                        <i class="fas fa-bars"></i>
                    </div>

                    <ul>
                        <li><a href="<?php echo INCLUDE_PATH;?>">Home</a></li>
                        <li><a href="<?php echo INCLUDE_PATH;?>depoimentos">Depoimentos</a></li>
                        <li><a href="<?php echo INCLUDE_PATH;?>servicos">Serviços</a></li>
                        <li><a href="<?php echo INCLUDE_PATH;?>contato">Contato</a></li>
                    </ul>
                </nav><!--mobile-->
                <div class="clear"></div><!--clear-->   
            </div><!--center-->
        </header>

        <?php

            if(file_exists('pages/'.$url.'.php')){
                include('pages/'.$url.'.php');
            }else{
                //Página inexistente
                if($url != 'depoimentos' && $url != 'servicos'){
                    $pagina404 = true;
                    include('pages/404.php');
                }else{
                    include('pages/home.php');
                }
            }
        ?>

        <footer <?php if(isset($pagina404) && $pagina404 == true) echo 'class="fixed"';?>>
            <div class="center">
                <p>Vinicius - Todos os direitos reservados</p>
            </div><!--center-->
        </footer>
        <script src="<?php echo INCLUDE_PATH;?>js/jquery.js"></script>
        <script src="<?php echo INCLUDE_PATH;?>js/scripts.js"></script>
        <script src="<?php echo INCLUDE_PATH;?>js/slider.js"></script>
    </body>
</html>