$(function(){
    $('nav.mobile').click(function(){
        //o que vai acontecer ao clicar na nav mobile!
        var listaMenu = $('nav.mobile ul');
        //Abrir menu através do fadeIn e fadeOut
        /*
        if(listaMenu.is(':hidden') == true)
            listaMenu.fadeIn();
        else
            listaMenu.fadeOut();*/
        
        //Abrir menu sem efeito
        /*
        if(listaMenu.is(':hidden') == true)
            listaMenu.show();
        else
            listaMenu.hide();*/

        //Abrir menu através do slidetoogle
        if(listaMenu.is(':hidden') == true){ //verificando se o menu está aberto
            //var icone = $('.bota-menu-mobile i'); também seria uma forma de fazer a linha de baixo
            var icone = $('.botao-menu-mobile').find('i');
            icone.removeClass('fa-bars');
            icone.addClass('fa-times');
            listaMenu.slideToggle();
        }
        else{
            var icone = $('.botao-menu-mobile i');
            icone.removeClass('fa-times');
            icone.addClass('fa-bars');
            listaMenu.slideToggle();
        }
    });

    if($('target').length > 0){
        //O elemento existe, portanto precisamos dar o scroll
        var elemento = '#'+$('target').attr('target');
        var divScroll = $(elemento).offset().top;
        $('html,body').animate({scrollTop:divScroll}, 2000);
    }
})